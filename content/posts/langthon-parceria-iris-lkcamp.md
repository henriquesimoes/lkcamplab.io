---
title: "Langthon: parceria Iris + LKCAMP"
date: 2023-04-29T11:22:00-03:00
draft: false
categories: ["Tutorial"]
tags: ["Langathon", "Iris", "Ciência de Dados"]
authors: ["Pedro Sader Azevedo"]
---

Neste evento, realizado em parceria com o Iris (entidade de ciência de dados da Unicamp), vamos aprender a usar e contribuir para o [LangFlow](https://github.com/logspace-ai/langflow), uma ferramenta *open source* de prototipação rápida de fluxos generativos usando modelos de Inteligência Artifical.

# Contribuindo para o LangFlow

Vamos contribuir para o LangFlow com exemplos válidos de uso da ferramenta. Esses exemplos são mostrados na interface do LangFlow, quando a opção de importar um fluxo é selecionada:

![home](/imgs/posts/langthon-parceria-iris-lkcamp/home.png)
![import](/imgs/posts/langthon-parceria-iris-lkcamp/import.png)
![examples](/imgs/posts/langthon-parceria-iris-lkcamp/examples.png)

Esses exemplos são armazenados no repositório [logspage-ai/langflow_examples](https://github.com/logspace-ai/langflow_examples), para o qual vamos contribuir. Para isso, siga os passos abaixo:

## 1. Criar uma conta no GitHub

Caso você não tenha uma conta no GitHub, clique [aqui](https://github.com/signup) e siga as instruções mostradas na página.

## 2. Gerar um *Personal Access Token*

Vamos gerar um *Persona Access Token* (PAT), para que seja possível autenticar sua conta no GitHub a partir do terminal. Para isso, acesse este [link](https://github.com/settings/tokens) e selecione a opção "Generate new token (classic)":

![new token](/imgs/posts/langthon-parceria-iris-lkcamp/new-token.png)

Em seguida, dê um nome descritivo ao seu PAT (ex: `evento-langthon`), escolha um prazo de validade (ex: `7 days`), e dê permissão de acesso aos seus repositórios:

![token setup](/imgs/posts/langthon-parceria-iris-lkcamp/token-setup.png)

Pronto, agora você tem seu PAT!

{{< alerts error "**Atenção**: o PAT será mostrado apenas uma vez. Certifique-se de salvá-lo em um lugar seguro para que você possa usá-lo mais tarde." >}}

## 3. Fazer um *fork* do repositório

Acesse o repositório [logspage-ai/langflow_examples](https://github.com/logspace-ai/langflow_examples) e clique em "Fork".

![fork](/imgs/posts/langthon-parceria-iris-lkcamp/fork.png)

Essa etapa é necessária para copiar o repositório para a sua própria conta do GitHub, assim você pode editá-lo à vontade.

## 4. Obter uma cópia local do seu *fork*

Para podermos editar o conteúdo do repositório, precisamos obter uma cópia dele no nosso próprio computador. O `git` tem essa funcionalidade embutida, basta rodar em um terminal:

```bash
git clone https://github.com/$NOME_DE_USUARIO_NO_GITHUB/langflow_examples
```

Substituindo `$NOME_DE_USUARIO_NO_GITHUB` com o seu nome de usuário no GitHub. Esse comando vai criar uma pasta chamada `langflow_examples` no seu diretório atual. Para entrar na pasta rode:

```bash
cd langflow_examples
```

## 5. Criar uma nova *branch* para suas mudanças

Vamos criar uma nova *branch* para as mudanças que vamos fazer no projeto. Isso permite que registremos nossas mudanças em um histórica paralelo ao histórico principal do projeto (que fica na branch `main`). Para criar e entrar numa nova *branch*, rode:

```bash
git checkout -b $NOME_DA_BRANCH
```

Substituindo `$NOME_DA_BRANCH` por um nome descritivo para a *branch* (ex: `add-new-example`).

{{< alerts info "**Nota**: criar uma nova branch nem sempre é estritamente necessário para fazer uma contribuição, mas é considerada uma boa prática no mundo open source.">}}

## 6. Exportar o seu fluxo como um arquivo .json

Para exportar seu fluxo, volte à página do LangFlow e clique em "Export":

![export](/imgs/posts/langthon-parceria-iris-lkcamp/export.png)

Na página de exportação, desmarque a opção "Save with API keys"

![exclude-api-key](/imgs/posts/langthon-parceria-iris-lkcamp/exclude-api-key.png)

{{< alerts error "**Atenção**: chaves de API são tipicamante confidenciais, então não deixe de desmarcar essa opção!" >}}

## 7. Mover o arquivo `.json` para a pasta correta

Mova o arquivo com extensão `.json` (que deve estar na sua pasta de Downloads), para a pasta `langflow_examples` que criamos na quarta etapa.

## 8. Adicionar e dar *commit* nas suas mudanças

Abra um terminal na pasta `langflow_examples` e digite:

```bash
git status
```

O output deste comando listará o arquivo `.json` que criamos na etapa anterior na seção de `Untracked files`. Isso significa que precisamos adicionar ao sistema de versionamento, o que pode ser realizado com o comando:

```bash
git add examples/$NOME_DO_ARQUIVO_JSON
```

Substituindo `$NOME_DO_ARQUIVO_JSON` com o nome do seu arquivo `.json`.

Agora vamos "registrar" essa mudança (adição do arquivo `.json`) no histórico de mudanças da *branch* que criamos na quinta etapa. Para isso, use o comando:

```bash
git commit -m "$MENSAGEM_DE_COMMIT"
```

Substituindo `$MENSAGEM_DE_COMMIT` por uma mensagem (em inglês) que explique resumidamente a sua mudança, por exemplo:

```bash
git commit -m "add example that uses the Calculator tool"
```

## 9. Subir suas mudanças para o GitHub

Agora vamos mandar as mudanças que fizemos localmente, rodando o comando:

```bash
git push origin $NOME_DA_BRANCH
```
Substituindo `$NOME_DA_BRANCH` pelo nome da *branch* que criamos na quarta etapa.

## 10. Fazer um pull request

Agora, na página do seu *fork* do repositório `langflow_examples`, você verá o popup abaixo, onde você deve clicar em "Compare & pull request":

![pr popup](/imgs/posts/langthon-parceria-iris-lkcamp/pr-popup.png)

Na tela de submissão do pull request, você pode adicionar uma descrição mais detalhada da sua mudança na caixa de texto grande. Feito isso, clique em "Create pull request":

![pr](/imgs/posts/langthon-parceria-iris-lkcamp/pr.png)

## 11. Esperar feedback

É possível que, para que sua contribuição seja aceita, algumas mudanças sejam requisitadas pelos mantenedores do projeto. Também é possível que ela seja aceita da maneira como está! Enfim, até lá, é só esperar!

---

Esperamos que você gostado de aprender mais sobre Inteligência Artifical e Open Source. Nos vemos na próxima! :penguin: :blue_heart:
